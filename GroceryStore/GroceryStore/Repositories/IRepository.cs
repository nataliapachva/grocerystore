﻿using System.Collections.Generic;

namespace GroceryStore.Repositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(string name);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
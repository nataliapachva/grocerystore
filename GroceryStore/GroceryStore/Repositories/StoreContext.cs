﻿using GroceryStore.Models;
using System.Collections.Generic;

namespace GroceryStore.Repositories
{
    public class StoreContext : IStoreContext
    {
        private StoreContext() { }

        public List<Product> Products { get; set; } = new List<Product>
        {
            new Product { Id = 1, Name = "Banana", Count = 2, Price = 0.99 },
            new Product { Id = 2, Name = "Orange", Count = 5, Price = 0.80 }
        };

        private static StoreContext storeContext;
        public static StoreContext Instance
        {
            get
            {
                if (storeContext == null)
                    storeContext = new StoreContext();

                return storeContext;
            }
        }
    }
}
﻿using GroceryStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroceryStore.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly IStoreContext storeContext;

        public ProductRepository() : this(StoreContext.Instance) { }

        public ProductRepository(IStoreContext storeContext)
        {
            this.storeContext = storeContext;
        }

        public IEnumerable<Product> GetAll()
        {
            return storeContext.Products;
        }

        public Product Get(string name)
        {
            Product product = storeContext.Products.SingleOrDefault(s => s.Name == name);

            return product;
        }

        public void Create(Product product)
        {
            Random rnd = new Random();
            product.Id = rnd.Next();
            storeContext.Products.Add(product);
        }

        public void Update(Product product)
        {
            Product old = storeContext.Products.SingleOrDefault(s => s.Id == product.Id);
            if (old != null)
            {
                old.Name = product.Name;
                old.Price = product.Price;
                old.Count = product.Count;
            }
            else
            {
                throw new Exception($"Not find product with Id={product.Id}");
            }
        }

        public void Delete(int id)
        {
            Product product = storeContext.Products.SingleOrDefault(s => s.Id == id);
            if (product != null)
            {
                storeContext.Products.Remove(product);
            }
            else
            {
                throw new Exception($"Not find product with such Id={product.Id}");
            }
        }
    }
}
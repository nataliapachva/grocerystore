﻿using GroceryStore.Models;
using System.Collections.Generic;

namespace GroceryStore.Repositories
{
    public interface IStoreContext
    {
        List<Product> Products { get; set; }
    }
}
﻿using GroceryStore.Models;
using System.Collections.Generic;

namespace GroceryStore.Managers
{
    public interface ISaleManager
    {
        double Sale(List<OrderProduct> orderProducts);
    }
}
﻿using GroceryStore.Models;
using GroceryStore.Parsers;
using GroceryStore.Repositories;
using GroceryStore.Validators;
using System;
using System.Collections.Generic;

namespace GroceryStore.Managers
{
    public class SaleManager : ISaleManager
    {
        private readonly IRepository<Product> productRepository;
        private readonly IValidator validator;
        private readonly IOrderProductParser orderProductParser;

        public SaleManager() : this(new OrderProductParser(), new Validator(), new ProductRepository()) { }

        public SaleManager(IOrderProductParser orderProductParser, IValidator validator, IRepository<Product> productRepository)
        {
            this.orderProductParser = orderProductParser;
            this.validator = validator;
            this.productRepository = productRepository;
        }

        public double Sale(List<OrderProduct> orderProducts)
        {
            double totalSum = 0;

            foreach (OrderProduct orderProduct in orderProducts)
            {
                Product prod = productRepository.Get(orderProduct.Name);
                int count = Convert.ToInt32(orderProduct.Count);
                prod.Count -= count;
                productRepository.Update(prod);
                totalSum = prod.Price * count;
            }

            return totalSum;
        }
    }
}
﻿namespace GroceryStore.Models
{
    public enum Command
    {
        Inventory,
        AddProduct,
        Incoming,
        Sale,
        Exit
    }
}
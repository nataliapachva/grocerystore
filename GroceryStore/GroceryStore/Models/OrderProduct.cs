﻿namespace GroceryStore.Models
{
    public class OrderProduct
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
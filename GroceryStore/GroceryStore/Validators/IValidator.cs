﻿using GroceryStore.Models;
using System;
using System.Collections.Generic;

namespace GroceryStore.Validators
{
    public interface IValidator
    {
        IList<Exception> Validate(List<OrderProduct> products);
    }
}
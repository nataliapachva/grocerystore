﻿using GroceryStore.Models;
using GroceryStore.Repositories;
using System;
using System.Collections.Generic;

namespace GroceryStore.Validators
{
    public class Validator : IValidator
    {
        private readonly IRepository<Product> productRepository;

        public Validator() : this(new ProductRepository()) { }

        public Validator(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public IList<Exception> Validate(List<OrderProduct> orders)
        {
            List<Exception> exceptions = new List<Exception>();
            foreach (OrderProduct order in orders)
            {
                Product product = productRepository.Get(order.Name);
                if (product == null)
                {
                    Exception ex = new Exception($"Not find product with name: {order.Name}");
                    exceptions.Add(ex);
                }
                else if (product.Count < order.Count)
                {
                    Exception ex = new Exception($"Not enough products with name: {product.Name}, count: {product.Count}");
                    exceptions.Add(ex);
                }
            }

            return exceptions;
        }
    }
}
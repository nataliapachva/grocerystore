﻿using GroceryStore.Models;

namespace GroceryStore.Handlers
{
    public interface IHandlerFactory
    {
        IHandler Get(Command command);
    }
}
﻿namespace GroceryStore.Handlers
{
    public interface IHandler
    {
        void Handle();
    }
}
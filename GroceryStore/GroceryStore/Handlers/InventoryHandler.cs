﻿using GroceryStore.Models;
using GroceryStore.Repositories;
using System;
using System.Collections.Generic;

namespace GroceryStore.Handlers
{
    public class InventoryHandler : IHandler
    {
        private readonly IRepository<Product> productRepository;

        public InventoryHandler() : this(new ProductRepository()) { }

        public InventoryHandler(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public void Handle()
        {
            IEnumerable<Product> products = productRepository.GetAll();
            Console.WriteLine("Count | Item    | Price per item");
            foreach (Product product in products)
            {
                Console.WriteLine(product);
            }
        }
    }
}
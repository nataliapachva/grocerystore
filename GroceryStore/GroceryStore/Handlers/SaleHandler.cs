﻿using GroceryStore.Managers;
using GroceryStore.Models;
using GroceryStore.Parsers;
using GroceryStore.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroceryStore.Handlers
{
    public class SaleHandler : IHandler
    {
        private readonly IValidator validator;
        private readonly IOrderProductParser orderProductParser;
        private readonly ISaleManager saleManager;

        public SaleHandler() : this(new OrderProductParser(), new Validator(), new SaleManager()) { }

        public SaleHandler(IOrderProductParser orderProductParser, IValidator validator, ISaleManager saleManager)
        {
            this.orderProductParser = orderProductParser;
            this.validator = validator;
            this.saleManager = saleManager;
        }

        public void Handle()
        {
            List<string> orders = ReadFromConsole();
            if (orders.Count == 0)
            {
                return;
            }

            List<OrderProduct> orderProducts = ParseOrders(orders);

            IList<Exception> exceptions = validator.Validate(orderProducts);

            if (exceptions.Count == 0)
            {
                double totalPrice = saleManager.Sale(orderProducts);

                Console.WriteLine($"Total price: {totalPrice}");
            }
            else
            {
                foreach (Exception ex in exceptions)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private List<string> ReadFromConsole()
        {
            string order = Console.ReadLine();
            List<string> orders = new List<string>();
            while (order != string.Empty)
            {
                orders.Add(order);
                order = Console.ReadLine();
            }

            return orders;
        }

        private List<OrderProduct> ParseOrders(List<string> orders)
        {
            List<OrderProduct> orderProducts = orders.Select(o => orderProductParser.ParseOrder(o)).ToList();

            return orderProducts;
        }
    }
}
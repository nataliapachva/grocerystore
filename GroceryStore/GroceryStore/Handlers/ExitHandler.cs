﻿using System;

namespace GroceryStore.Handlers
{
    public class ExitHandler : IHandler
    {
        public void Handle()
        {
            Environment.Exit(0);
        }
    }
}
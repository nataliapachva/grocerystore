﻿using GroceryStore.Models;

namespace GroceryStore.Handlers
{
    public class HandlerFactory : IHandlerFactory
    {
        public IHandler Get(Command command)
        {
            switch (command)
            {
                case Command.Inventory:
                    return new InventoryHandler();
                case Command.AddProduct:
                    return new AddProductHandler();
                case Command.Incoming:
                    return new IncomingHandler();
                case Command.Sale:
                    return new SaleHandler();
                case Command.Exit:
                    return new ExitHandler();
                default:
                    return null;
            }
        }
    }
}
﻿using GroceryStore.Models;
using GroceryStore.Repositories;
using System;

namespace GroceryStore.Handlers
{
    public class AddProductHandler : IHandler
    {
        private readonly IRepository<Product> productRepository;

        public AddProductHandler() : this(new ProductRepository()) { }

        public AddProductHandler(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public void Handle()
        {
            Console.Write("Name: ");
            string name = Console.ReadLine();
            Console.Write("Price: ");
            double price = Convert.ToDouble(Console.ReadLine());
            Product newProduct = new Product { Name = name, Price = price };
            productRepository.Create(newProduct);
        }
    }
}
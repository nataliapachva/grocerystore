﻿using GroceryStore.Models;
using GroceryStore.Repositories;
using System;

namespace GroceryStore.Handlers
{
    public class IncomingHandler : IHandler
    {
        private readonly IRepository<Product> productRepository;

        public IncomingHandler() : this(new ProductRepository()) { }

        public IncomingHandler(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public void Handle()
        {
            string line = Console.ReadLine();
            while (line != string.Empty)
            {
                string[] words = line.Split(new string[] { ": " }, StringSplitOptions.None);
                Product product = productRepository.Get(words[0]);
                product.Count += Convert.ToInt32(words[1]);
                productRepository.Update(product);
                line = Console.ReadLine();
            }
        }
    }
}
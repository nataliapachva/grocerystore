﻿using GroceryStore.Models;
using System;

namespace GroceryStore.Parsers
{
    public class OrderProductParser : IOrderProductParser
    {
        public OrderProduct ParseOrder(string line)
        {
            string[] words = line.Split(new string[] { ": " }, StringSplitOptions.None);
            OrderProduct orderProduct = new OrderProduct { Name = words[0], Count = Convert.ToInt32(words[1]) };

            return orderProduct;
        }
    }
}
﻿using GroceryStore.Models;

namespace GroceryStore.Parsers
{
    public interface IOrderProductParser
    {
        OrderProduct ParseOrder(string line);
    }
}
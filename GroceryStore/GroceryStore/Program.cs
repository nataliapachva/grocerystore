﻿using GroceryStore.Handlers;
using GroceryStore.Models;
using GroceryStore.Repositories;
using System;

namespace GroceryStore
{
    class Program
    {
        public static void ListenCommands(IRepository<Product> productRepository)
        {
            Console.WriteLine("Write command: inventory, addproduct, incoming, sale, exit");
            IHandlerFactory handlerFactory = new HandlerFactory();

            while (true)
            {
                Console.Write("> ");
                string commandStr = Console.ReadLine();
                Command command;
                if (Enum.TryParse(commandStr, true, out command))
                {
                    IHandler handler = handlerFactory.Get(command);
                    handler.Handle();
                }
                else
                {
                    Console.WriteLine("Write correct command!");
                }
            };
        }

        static void Main(string[] args)
        {
            try
            {
                IRepository<Product> productRepository = new ProductRepository();
                ListenCommands(productRepository);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}